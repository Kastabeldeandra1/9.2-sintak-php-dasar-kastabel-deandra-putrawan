<?php
    $nim = "2300018322";
    $nama = "Kastabel Deandra Putrawan";
    $fakultas = "Teknologi Industri";
    $prodi = "Informatika";
    $umur = 18;
    $ipk = 4.00;
    $status = TRUE;
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP Dasar</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="informasi">
        <h2>INFORMASI MAHASISWA UAD</h2>
            <p>Nama : <?php echo $nama?></p>
            <p>NIM : <?php echo $nim?></p>
            <p>Fakultas : <?php echo $fakultas?></p>
            <p>Program Studi : <?php echo $prodi?></p>
            <p>Umur : <?php echo $umur?></p>
            <p>IPK : <?php echo number_format($ipk, 2)?></p>
            <p>Status : <?php
            if ($status = TRUE){  
                echo "Aktif";
            }
            else {
                echo "Tidak aktif";
            }
        ?></p>
    </div>
</body>
</html>

